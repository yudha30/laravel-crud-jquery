<ul>
    <li>Clone this Project</li>
    <li>copy .env.example to .env</li>
    <li>Setting database</li>
    <li>clear cache & config (php artisan config:clear)</li>
    <li>composer install</li>
    <li>install yajra/datatables</li>
</ul>