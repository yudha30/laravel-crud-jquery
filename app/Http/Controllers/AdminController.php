<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin;
use DataTables;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $simpan = new Admin;
            $simpan->name = $request->name;
            $simpan->alamat = $request->alamat;
            $simpan->save();

            return response([
                'status'=>200,
                'message' => 'Data Tersimpan'
            ]);
        }catch(Exception $e){
            return response([
                'status'=>400,
                'message' => $e.getMessage()
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{

            $user = Admin::find($id);

            $user->name = $request->name;
            $user->alamat = $request->alamat;
            $user->save();

            return response([
                'status' => 200,
                'message' => 'Data Diupdate'
            ]);
        }catch(Exception $e){
            return response([
                'status' => 400 ,
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $user = Admin::find($id)->delete();

            return response([
                'status' => 200,
                'message' => 'Data Dihapus'
            ]);

        }catch(Exception $e){
            return response([
                'status' => 400,
                'message' => $e->getMessage()
            ]);
        }
    }

    // ----------------------------- non resource -------------------------
    public function getData(){
        $admin = Admin::all();
        return DataTables::of($admin)->addIndexColumn()->make(true);
    }
}
