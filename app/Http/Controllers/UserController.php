<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use DataTables;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('user.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $simpan = new User;
            $simpan->name = $request->name;
            $simpan->email = $request->email;
            $simpan->save();

            return response([
                'status'=>200,
                'message' => 'Data Tersimpan'
            ]);
        }catch(Exception $e){
            return response([
                'status'=>400,
                'message' => $e.getMessage()
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{

            $user = User::find($id);

            $user->name = $request->name;
            $user->email = $request->email;
            $user->save();

            return response([
                'status' => 200,
                'message' => 'Data Diupdate'
            ]);
        }catch(Exception $e){
            return response([
                'status' => 400 ,
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $user = User::find($id)->delete();

            return response([
                'status' => 200,
                'message' => 'Data Dihapus'
            ]);

        }catch(Exception $e){
            return response([
                'status' => 400,
                'message' => $e->getMessage()
            ]);
        }
    }

    // ------------------- non resource -----------------------
    public function getdata(){
        $user = User::all();
        return DataTables::of($user)->addIndexColumn()->make(true);
    }
}
