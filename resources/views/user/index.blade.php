@extends('layouts.app')
@section('content')
<div class="container mt-5">
    <button class="btn btn-primary mb-3" id="add_button">Tambah</button>
    <div class="card">
        <div class="card-body">
            <div class="row d-flex justify-content-between">
                <div class="col-5 d-flex">
                    Show Data :
                    <div class="row">
                        <div class="col">
                            <div id="show_data" class="d-flex ml-2">
                                <select class="form-control form-control-sm" id="pageLength">
                                    <option>25</option>
                                    <option>50</option>
                                    <option>100</option>
                                  </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <input type="text" class="form-control form-control-sm" id="search" aria-describedby="emailHelp" placeholder="Search">
                </div>
            </div>
            <div class="row mt-3">
                <div class="col">
                    <table class="table" id="init-table">
                        <thead class="" width="100%">
                            <tr>
                                <th>#</th>
                                <th width="40%">Nama</th>
                                <th width="40%">Email</th>
                                <th width="20%">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@include('user.modal')

@push('js')
    @include('user.script')
@endpush
