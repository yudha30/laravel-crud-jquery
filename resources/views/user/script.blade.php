<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function () {
        Page.init();
    });

    var Page = function () {
        var _componentPage = function () {
            var init_table;
            $(document).ready(function () {
                initTable();
                initAction();
                formSubmit();
            });

            const initTable = () => {
                init_table = $('#init-table').DataTable({
                        destroy: true,
                        processing: true,
                        responsive: true,
                        serverSide: true,
                        sScrollY: ($(window).height() < 700) ? $(window).height() - 200 : $(window)
                            .height() - 400,
                        ajax: {
                            type: 'POST',
                            url: "{{ url('user/data') }}",
                        },
                        columns: [{
                                data: 'DT_RowIndex'
                            },
                            {
                                data: 'name'
                            },
                            {
                                data: 'email'
                            },
                            {
                                defaultContent: ''
                            }
                        ],
                        columnDefs: [{
                                targets: 0,
                                searchable: false,
                                orderable: false,
                                className: "text-center"
                            },
                            {
                                targets: -1,
                                searchable: false,
                                orderable: false,
                                className: "text-center",
                                data: "id",
                                render: function (data, type, full, meta) {
                                    return `
                                    <a class="btn btn-info btn-sm btn-edit" href="{{url('/user')}}/${data}">Edit</a>
                                    <a class="btn btn-danger btn-sm btn-delete" href="{{url('/user')}}/${data}">Hapus</a>
                                `
                                }
                            },
                        ],
                        order: [
                            [1, 'asc']
                        ],
                        searching: true,
                        paging: true,
                        lengthChange: false,
                        bInfo: true,
                        dom: '<"datatable-header"><tr><"datatable-footer"ip>',
                        language: {
                            search: '<span>Search:</span> _INPUT_',
                            searchPlaceholder: 'Search.',
                            lengthMenu: '<span>Show:</span> _MENU_',
                            processing: '<div class="text-center"> <div class="spinner-border text-primary" role="status"> <span class="sr-only">Loading...</span> </div> </div>',
                        },
                    }),

                    initAction = () => {
                        $(document).on('click', '#add_button', function (event) {
                            event.preventDefault();
                            $('#formAddUser').trigger('reset');
                            $('#formAddUser').attr('action', '{{url('user ')}}');
                            $('#formAddUser').attr('method', 'POST');

                            showModal('modal_user');
                        });

                        $(document).on('click', '.btn-edit', function (event) {
                            event.preventDefault();

                            var user = init_table.row($(this).parents('tr')).data();
                            // console.log(user);
                            $('#formAddUser').trigger('reset');
                            $('#formAddUser').attr('action', $(this).attr('href'));
                            $('#formAddUser').attr('method', 'PATCH');

                            $('#formAddUser').find('input[name="name"]').val(user.name);
                            $('#formAddUser').find('input[name="email"]').val(user.email);
                            showModal('modal_user');
                        });

                        $(document).on('click', '.btn-delete', function (event) {
                            event.preventDefault();
                            var url = $(this).attr('href');
                            Swal.fire({
                                title: 'Apakah Anda Yakin?',
                                text: "Data ini akan terhapus secara permanen",
                                icon: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Ya, Hapus!',
                                cancelButtonText: 'Batal'
                            }).then((result) => {
                                if (result.value) {
                                    $.ajax({
                                        url : url,
                                        type : 'DELETE',
                                        dataType : 'json'
                                    })
                                    .done(function (res, xhr, meta){
                                        if(res.status == 200){
                                            toastr.success(res.message,'Success');
                                            init_table.draw(false);
                                        }
                                    })
                                    .fail(function(res, error){
                                        toastr.error(res.responseJSON.message, 'Gagal');
                                    })
                                    .always(function(){})
                                }
                            })
                        })
                    },

                    formSubmit = () => {
                        $('#formAddUser').submit(function (event) {
                            event.preventDefault();
                            $.ajax({
                                    url: $(this).attr('action'),
                                    type: $(this).attr('method'),
                                    data: $(this).serialize()
                                })
                                .done(function (res, xhr, meta) {
                                    if (res.status == 200) {
                                        toastr.success(res.message, 'Success');
                                        init_table.draw(false);
                                        hideModal('modal_user');
                                    }
                                })
                                .fail(function (res, error) {
                                    toastr.error(res.responseJSON.message, 'Gagal')
                                })
                                .always(function () {})
                        });
                    }

                $('#search').on('keyup', function () {
                    init_table.search(this.value).draw();
                });

                $('#pageLength').on('change', function () {
                    init_table.page.len(this.value).draw();
                });

                $(document).ready(function(){
                    var pageLen = $('#pageLength').text();
                    init_table.page.len(pageLen).draw();
                })
            };

            const showModal = function (selector) {
                    $('#' + selector).modal('show')
                },
                hideModal = function (selector) {
                    $('#' + selector).modal('hide')
                }
        };

        return {
            init: function () {
                _componentPage();
            }
        }
    }();

</script>
