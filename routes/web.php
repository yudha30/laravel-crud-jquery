<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/user');
});

Route::post('user/data','UserController@getData');
Route::resource('user', 'UserController');

Route::post('admin/data', 'AdminController@getData');
Route::resource('admin', 'AdminController');
